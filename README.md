# TD 8

Framework sass pour des générations de classes à la manière de Bootstrap

## Prérequis

- Avoir [SASS](https://sass-lang.com/install) d'installé.

## Utilisation

Pour modifier le framework, rien de plus simple !
<br>
<br>
Rendez-vous dans le fichier "framework.scss" pour modifier le framework

```
src/scss/framework.scss
```

La modification des variables se fait dans le fichier "_variables.scss" de ce même répertoire (scss)
<br>
<br>
Pour générer le nouveau framework modifié par vos soins il est nécessaire de le compiler en SASS. 
Pour se faire, on utilise la commande suivante (à la racine du projet) :

```
sass src/scss/framework.scss src/css/<nom fichier généré>.css
```

Une fois ce CSS généré il suffit de l'appeler dans une de vos pages Web pour vous en servir.

## Informations
Deux fichiers HTML sont disponible pour représenter 2 générations différentes du framework:

```
src/html/
```

## Développeurs

* **Thomas Serres**
* **Clément Mercier**
